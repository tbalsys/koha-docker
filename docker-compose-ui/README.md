# Web UI for docker containers

Create a password file:
```
# htpasswd -c .htpasswd user1
# htpasswd .htpasswd user2
```

Create a virtual sub-server for in Apache and add some additional configuration.
Add this to virtual server http port 80 configuration to redirect everything except certificate renewals to https:

```
RewriteEngine on
RewriteCond %{HTTPS} off
RewriteCond %{REQUEST_URI} !^/\.well\-known
RewriteRule ^(.*)$ https://%{HTTP_HOST}%{REQUEST_URI} [L,R=301]
```

And this to virtual server https port 443 configuration to create password protected reverse proxy to the docker-compose-ui service:

```
ProxyPass / http://localhost:5000/
ProxyPassReverse / http://localhost:5000/
<Proxy *>
AuthType Basic
AuthName "Restricted Content"
AuthUserFile /opt/docker/docker-compose-ui/.htpasswd
Require valid-user
allow from all
</Proxy>
```
