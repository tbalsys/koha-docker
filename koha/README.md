# Koha docker image

- Edit variables in `.env`
- Run `create_db.sh` on machine hosting the database server
- Import the database dump into the newly created DB
- Run `docker-compose up`
- To recreate indexes remove the *indexes* volume: `docker volume rm koha_indexes`
