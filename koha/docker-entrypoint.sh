#!/bin/bash
set -e

echo "Database server check ..."
if ! ping -c 1 -W 1 $KOHA_DBHOST; then
    echo "ERROR: Could not connect to remote mysql '$KOHA_DBHOST'"
    exit 1
fi

if [ ! -f /etc/supervisor/conf.d/supervisord.conf ]; then
    echo "Global config ..."
    envsubst < /templates/koha-sites.conf.tmpl > /etc/koha/koha-sites.conf
    envsubst < /templates/passwd.tmpl > /etc/koha/passwd
    envsubst < /templates/supervisord.conf.tmpl > /etc/supervisor/conf.d/supervisord.conf
    envsubst < /templates/koha-common.cnf.tmpl > /etc/mysql/koha-common.cnf
   
    service memcached start

    echo "Configuring local instance ..."
    echo -e "Listen $KOHA_INTRAPORT\nListen $KOHA_OPACPORT" | tee /etc/apache2/ports.conf
    echo "ServerName ${KOHA_INSTANCE}${KOHA_DOMAIN}" | tee /etc/apache2/conf-available/fqdn.conf
    a2enconf fqdn
    
    echo "Initializing local instance ..."
    koha-create --request-db $KOHA_INSTANCE || true
    koha-create --populate-db $KOHA_INSTANCE
    koha-upgrade-schema $KOHA_INSTANCE
    koha-enable $KOHA_INSTANCE
    
    digest=$(echo -n "$KOHA_ADMINPASS" |
                      perl -e '
                            use Digest::MD5 qw(md5_base64); 
                            while (<>) { print md5_base64($_), "\n"; }')
    echo "UPDATE borrowers SET password = '$digest' WHERE borrowernumber = 1" |
        mysql -h $KOHA_DBHOST -u $KOHA_ADMINUSER -p$KOHA_ADMINPASS \
            koha_$KOHA_INSTANCE

    echo "Configuring languages ($INSTALL_LANGUAGES) ..."
    for language in $INSTALL_LANGUAGES
    do
        if ! koha-translate --list | grep $language; then
            KOHA_CONF=/etc/koha/sites/$KOHA_INSTANCE/koha-conf.xml koha-translate --install $language
        fi
    done

    echo "Installation finished - Stopping all services and giving supervisord control ..."
    service apache2 stop
    koha-indexer --stop "$KOHA_INSTANCE"
    koha-stop-zebra "$KOHA_INSTANCE"
    killall memcached
fi

if [ -z "$(ls -A /var/lib/koha/$KOHA_INSTANCE/biblios/register)" ]; then
    echo "Reindexing ..."
    koha-rebuild-zebra --full $KOHA_INSTANCE
fi

exec supervisord -c /etc/supervisor/conf.d/supervisord.conf
